Dev notes:
1. To configure use json file TvMaze\TvMaze.Common\appConfig.json or cmd params or environment variables. It's just for developers and testing in real environments Azure app config or AWS app config centralized storage has to be used.
2. To run crawler rebuild TvMaze.Host.csproj it's console app and run it.
3. To run asp.net web api rebuild TvMaze.WebHost and use swagger https://localhost/swagger or any other tools like Postman, curl example below:
```
curl -X 'GET' \
  'https://localhost/TvMazeShows?pageNum=0&pageSize=50' \
  -H 'accept: text/plain'
```


To run Mongo DB you can run it in container (that's for development only, for real usage running DB engines in containers are not good):
```
docker run -d -p 27017:27017 --name test-mongo mongo:latest
```

