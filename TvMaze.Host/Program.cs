﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TvMaze.Common.Configuration.Extensions;
using TvMaze.Host;

var hostBuilder = Host.CreateDefaultBuilder(args);
hostBuilder.TvMazeHostBuild(args, "CrawlerHost");
hostBuilder.ConfigureServices(x => x.AddHostedService<CrawlerBackgroundService>());

var app = hostBuilder.Build();
app.Run();