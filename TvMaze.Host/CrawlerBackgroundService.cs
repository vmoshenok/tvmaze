﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using TvMaze.Lib.TvShows.Services;

namespace TvMaze.Host
{
    internal class CrawlerBackgroundService : BackgroundService
    {
        private readonly ICrawlerService _crawlerService;
        private readonly ILogger _logger;

        public CrawlerBackgroundService(ICrawlerService crawlerService, ILogger logger)
        {
            _crawlerService = crawlerService;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                await _crawlerService.Run(false, stoppingToken);
                _logger.LogInformation("Crawler job completed successfully. Re-run after 1 minute.");
                await Task.Delay(60000, stoppingToken);
            }
        }
    }
}
