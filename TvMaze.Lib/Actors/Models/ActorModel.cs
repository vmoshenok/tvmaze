﻿using MongoDB.Bson.Serialization.Attributes;
using TvMaze.Common.Configuration.Serializers;
using TvMaze.Common.Models;

namespace TvMaze.Lib.Actors.Models
{
    public class ActorModel : BaseModel
    {
        public string Name { get; set; }
        [BsonSerializer(typeof(BsonDateOnlySerializer))]
        public DateOnly BirthDay { get; set; }
    }
}
