﻿namespace TvMaze.Lib.TvShows.Services;

public interface ICrawlerService
{
    Task Run(bool forceRecrawl, CancellationToken cancellationToken);
}