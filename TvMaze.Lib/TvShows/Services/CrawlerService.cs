﻿using Microsoft.Extensions.Logging;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using TvMaze.Lib.TvShows.Models;
using TvMaze.Lib.TvShows.Repositories;

namespace TvMaze.Lib.TvShows.Services
{
    public class CrawlerService : ICrawlerService
    {
        private readonly TvMazeRestRepository _tvMazeRestRepository;
        private readonly TvMazeMongoRepository _tvMazeMongoRepository;
        private readonly ILogger _logger;

        public CrawlerService(TvMazeRestRepository tvMazeRestRepository, TvMazeMongoRepository tvMazeMongoRepository, ILogger logger)
        {
            _tvMazeRestRepository = tvMazeRestRepository;
            _tvMazeMongoRepository = tvMazeMongoRepository;
            _logger = logger;
        }

        public async Task Run(bool forceRecrawl, CancellationToken cancellationToken)
        {
            var showsWithCastCollection = _tvMazeMongoRepository.GetCollection<ShowWithCastModel>();
            var shows = new List<ShowWithCastModel>();
            var existedIds = (await showsWithCastCollection.AsQueryable().Select(x=>x.Id).ToListAsync(cancellationToken)).ToHashSet();

            for (var i = 0;; i++)
            {
                var showsAtPage = await _tvMazeRestRepository.GetShows(i, cancellationToken);
                if(showsAtPage == null || showsAtPage.Length == 0) break;
                foreach (var show in showsAtPage)
                {
                    if(!forceRecrawl && existedIds.Contains(show.Id)) continue;
                    shows.Add(new ShowWithCastModel(show));
                }
                _logger.LogDebug($"Successfully read shows page: {i}");
            }

            await Parallel.ForEachAsync(shows, cancellationToken, async (show, token) =>
            {
                var actors = await _tvMazeRestRepository.GetCastActors((int)show.Id, token);
                if (actors is {Length: > 0})
                {
                    show.Cast.AddRange(actors);
                }
                await showsWithCastCollection.ReplaceOneAsync(x => x.Id == show.Id, show, new ReplaceOptions { IsUpsert = true }, cancellationToken);

                _logger.LogDebug($"Successfully read and saved cast for showId: {show.Id}");
            });


        }
    }
}
