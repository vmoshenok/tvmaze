﻿using TvMaze.Lib.Actors.Models;

namespace TvMaze.Lib.TvShows.Models
{
    public class ShowWithCastModel : ShowModel
    {
        public ShowWithCastModel()
        {
        }

        public ShowWithCastModel(ShowModel show)
        {
            Id = show.Id;
            Name = show.Name;
        }

        public List<ActorModel> Cast { get; set; } = new();
    }
}
