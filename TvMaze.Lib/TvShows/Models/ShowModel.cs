﻿using TvMaze.Common.Models;

namespace TvMaze.Lib.TvShows.Models
{
    public class ShowModel : BaseModel
    {
        public string Name { get; set; }
    }
}
