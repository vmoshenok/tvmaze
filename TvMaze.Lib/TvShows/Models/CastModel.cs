﻿using TvMaze.Lib.Actors.Models;

namespace TvMaze.Lib.TvShows.Models
{
    public class CastModel
    {
        public ActorModel Person { get; set; }
        public ActorModel Character { get; set; }
        public bool Self { get; set; }
        public bool Voice { get; set; }
    }
}
