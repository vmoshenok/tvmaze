﻿using TvMaze.Common.Repository;
using TvMaze.Lib.Actors.Models;
using TvMaze.Lib.TvShows.Models;

namespace TvMaze.Lib.TvShows.Repositories;

public interface ITvMazeRestRepository : IBaseRestRepository
{
    Task<ShowModel[]?> GetShows(int page, CancellationToken cancellationToken);
    Task<ActorModel[]?> GetCastActors(int showId, CancellationToken cancellationToken);
}