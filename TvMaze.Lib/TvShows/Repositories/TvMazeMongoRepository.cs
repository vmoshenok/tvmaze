﻿using TvMaze.Common.Configuration.Models;
using TvMaze.Common.Repository;

namespace TvMaze.Lib.TvShows.Repositories
{
    public class TvMazeMongoRepository : BaseMongoDbRepository, ITvMazeMongoRepository
    {
        public TvMazeMongoRepository(AppConfig configuration) : base(configuration)
        {
        }
    }
}
