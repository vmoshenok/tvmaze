﻿using TvMaze.Common.Repository;

namespace TvMaze.Lib.TvShows.Repositories;

public interface ITvMazeMongoRepository : IBaseMongoDbRepository
{
}