﻿using System.Net;
using Microsoft.Extensions.Logging;
using RestSharp;
using TvMaze.Common.Configuration.Models;
using TvMaze.Common.Repository;
using TvMaze.Lib.Actors.Models;
using TvMaze.Lib.TvShows.Models;
using ILogger = Microsoft.Extensions.Logging.ILogger;

namespace TvMaze.Lib.TvShows.Repositories
{
    public class TvMazeRestRepository : BaseRestRepository, ITvMazeRestRepository
    {
        private readonly ILogger _logger;

        public TvMazeRestRepository(AppConfig appConfig, ILogger logger) : base(appConfig.TvMazeApiUrl, logger)
        {
            _logger = logger;
        }

        public async Task<ShowModel[]?> GetShows(int page, CancellationToken cancellationToken)
        {
            var request = new RestRequest("/shows");
            request.AddParameter("page", page);
            var response = await RetryRestCall(cancellationToken,
                async () => await Client.ExecuteGetAsync<ShowModel[]?>(request, cancellationToken),
                restResponse => restResponse.IsSuccessful || restResponse.StatusCode == HttpStatusCode.NotFound);

            if (response?.ErrorException != null) _logger.LogError(response.ErrorException.ToString());

            return response?.Data;
        }

        public async Task<ActorModel[]?> GetCastActors(int showId, CancellationToken cancellationToken)
        {
            var request = new RestRequest($"/shows/{showId}/cast");
            var response = await RetryRestCall(cancellationToken,
                async () => await Client.ExecuteGetAsync<CastModel[]?>(request, cancellationToken),
                restResponse => restResponse.IsSuccessful || restResponse.StatusCode == HttpStatusCode.NotFound);

            if(response?.ErrorException != null) _logger.LogError(response.ErrorException.ToString());

            return response?.Data?.Select(x => x.Person).ToArray();
        }
    }
}
