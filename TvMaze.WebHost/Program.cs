using System.Net.Mime;
using Microsoft.AspNetCore.Diagnostics;
using Newtonsoft.Json;
using TvMaze.Common.Configuration.Extensions;
using TvMaze.WebHost;

var builder = WebApplication.CreateBuilder(args);
builder.Host.TvMazeHostBuild(args, "WebHost");
builder.Services.AddControllers()
    .AddJsonOptions(options => options.JsonSerializerOptions.Converters.Add(new JsonDateOnlyConverter()));
builder.Services.AddSwaggerGen();
builder.Services.AddResponseCompression(options =>
{
    options.EnableForHttps = true;
});
builder.WebHost.UseDefaultServiceProvider(x => x.ValidateOnBuild = false);
var app = builder.Build();

app.UseHttpsRedirection();
app.UseResponseCompression();
app.MapControllers();

app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("v1/swagger.json", "API V1");
});

app.UseExceptionHandler(exceptionHandlerApp =>
{
    exceptionHandlerApp.Run(async context =>
    {
        var error = context.Features.Get<IExceptionHandlerFeature>();
        if (error != null)
        {
            if (error.Error is UnauthorizedAccessException) context.Response.StatusCode = StatusCodes.Status401Unauthorized;
            context.Response.ContentType = MediaTypeNames.Application.Json;
            await context.Response.WriteAsync(JsonConvert.SerializeObject(error.Error));
        }
    });
});

app.Run();