﻿namespace TvMaze.WebHost.Models
{
    public class PagedResponse<T>
    {
        public int PageNum { get; set; }
        public int PageSize { get; set; }
        public int TotalPages { get; set; }
        public T[] Data { get; set; }
    }
}
