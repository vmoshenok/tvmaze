using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using TvMaze.Lib.TvShows.Models;
using TvMaze.Lib.TvShows.Repositories;
using TvMaze.WebHost.Models;

namespace TvMaze.WebHost.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TvMazeShowsController : ControllerBase
    {
        private readonly ITvMazeMongoRepository _tvMazeMongoRepository;

        public TvMazeShowsController(ITvMazeMongoRepository tvMazeMongoRepository)
        {
            _tvMazeMongoRepository = tvMazeMongoRepository;
        }

        [HttpGet]
        [Route("")]
        public async Task<PagedResponse<ShowWithCastModel>> GetPages(int pageNum = 0, int pageSize = 50)
        {
            if (pageSize <= 0) pageSize = 50;
            if (pageNum < 0) pageNum = 0;
            var showsCollection = _tvMazeMongoRepository.GetCollection<ShowWithCastModel>();
            var result = await showsCollection.Find(FilterDefinition<ShowWithCastModel>.Empty).Skip(pageNum * pageSize).Limit(pageSize).ToListAsync();
            var totalItems = await showsCollection.CountDocumentsAsync(FilterDefinition<ShowWithCastModel>.Empty);

            foreach (var showWithCastModel in result)
            {
                showWithCastModel.Cast = showWithCastModel.Cast.OrderByDescending(x => x.BirthDay).ToList();
            }

            return new PagedResponse<ShowWithCastModel>
            {
                Data = result.ToArray(),
                PageNum = pageNum,
                PageSize = pageSize,
                TotalPages = (int) totalItems / pageSize
            };
        }
    }
}