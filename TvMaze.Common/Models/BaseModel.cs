﻿using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace TvMaze.Common.Models
{
    public class BaseModel
    {
        [BsonId]
        [BsonIgnoreIfDefault]
        [JsonIgnore]
        public ObjectId ObjectId { get; set; }
        public long Id { get; set; }
    }
}
