﻿using MongoDB.Driver;
using TvMaze.Common.Models;

namespace TvMaze.Common.Repository;

public interface IBaseMongoDbRepository
{
    IMongoCollection<T> GetCollection<T>();
    Task<T> GetById<T>(string id) where T : BaseModel;
    Task<T> Create<T>(T obj);
    Task<T> Update<T>(T obj) where T : BaseModel;
}