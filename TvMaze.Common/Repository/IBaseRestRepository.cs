﻿using RestSharp;

namespace TvMaze.Common.Repository;

public interface IBaseRestRepository
{
    Task<RestResponse<T>?> RetryRestCall<T>(CancellationToken cancellationToken, Func<Task<RestResponse<T>>> operation, Func<RestResponse<T>, bool> retryTrigger, int maxRetry = 0, int delay = 100);
}