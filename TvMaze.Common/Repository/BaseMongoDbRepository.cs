﻿using System.Security.Authentication;
using MongoDB.Bson;
using MongoDB.Driver;
using TvMaze.Common.Configuration.Models;
using TvMaze.Common.Models;

namespace TvMaze.Common.Repository
{
    public abstract class BaseMongoDbRepository : IBaseMongoDbRepository
    {
        private readonly IMongoDatabase _db;

        protected BaseMongoDbRepository(AppConfig configuration)
        {
            string connectionString = configuration.DbConnectionStr;
            var settings = MongoClientSettings.FromUrl(new MongoUrl(connectionString));
            settings.SslSettings = new SslSettings { EnabledSslProtocols = SslProtocols.Tls12 };
            var mongoClient = new MongoClient(settings);
            _db = mongoClient.GetDatabase($"TvMaze_{configuration.Env}");
        }

        public IMongoCollection<T> GetCollection<T>()
        {
            return _db.GetCollection<T>(typeof(T).Name);
        }

        public async Task<T> GetById<T>(string id) where T : BaseModel
        {
            var objId = ObjectId.Parse(id);
            return await GetCollection<T>().Find(x => x.ObjectId == objId).FirstOrDefaultAsync();
        }

        public async Task<T> Create<T>(T obj)
        {
            await GetCollection<T>().InsertOneAsync(obj);
            return obj;
        }

        public async Task<T> Update<T>(T obj) where T : BaseModel
        {
            await GetCollection<T>().ReplaceOneAsync(x => x.Id == obj.Id, obj);
            return obj;
        }
    }
}
