﻿using Microsoft.Extensions.Logging;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;
using TvMaze.Common.Configuration.Models;

namespace TvMaze.Common.Repository
{
    public abstract class BaseRestRepository : IBaseRestRepository
    {
        private readonly ILogger _logger;
        protected RestClient Client { get; }

        protected BaseRestRepository(string apiUrl, ILogger logger)
        {
            _logger = logger;
            Client = string.IsNullOrEmpty(apiUrl) ? new RestClient() : new RestClient(apiUrl);
            Client.UseNewtonsoftJson(Settings.JsonSerializerSettings);
            Client.Options.FailOnDeserializationError = false;
        }

        public async Task<RestResponse<T>?> RetryRestCall<T>(CancellationToken cancellationToken, Func<Task<RestResponse<T>>> operation, Func<RestResponse<T>, bool> retryTrigger, int maxRetry = 0, int delay = 100)
        {
            for (var i = 0; i < maxRetry || maxRetry == 0; i++)
            {
                try
                {
                    var result = await operation();
                    if(retryTrigger(result)) return result;
                    _logger.LogDebug($"Rest call will be retried, response status code: {result.StatusCode}, response content: {result.Content}");
                }
                catch (Exception e)
                {
                    _logger.LogError(e.ToString());
                }

                await Task.Delay(delay, cancellationToken);
            }

            return null;
        }
    }
}
