﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;

namespace TvMaze.Common.Configuration.Serializers
{
    public class BsonDateOnlySerializer : IBsonSerializer<DateOnly>
    {
        object IBsonSerializer.Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            return Deserialize(context, args);
        }

        public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, DateOnly value)
        {
            BsonSerializer.Serialize(context.Writer, value);
        }

        public DateOnly Deserialize(BsonDeserializationContext context, BsonDeserializationArgs args)
        {
            var d = BsonSerializer.Deserialize<BsonDocument>(context.Reader);

            var year = Convert.ToInt32(d["Year"]);
            var month = Convert.ToInt32(d["Month"]);
            var day = Convert.ToInt32(d["Day"]);

            return new DateOnly(year, month, day);
        }

        public void Serialize(BsonSerializationContext context, BsonSerializationArgs args, object value)
        {
            BsonSerializer.Serialize(context.Writer, value);
        }

        public Type ValueType => typeof(DateOnly);
    }
}
