﻿using Microsoft.Extensions.Configuration;

namespace TvMaze.Common.Configuration.Extensions
{
    public static class ConfigurationBuilderExtensions
    {
        public static IConfigurationBuilder AddTvMazeConfiguration(this IConfigurationBuilder configurationBuilder, string[]? args)
        {
            configurationBuilder
                .AddEnvironmentVariables()
                .AddCommandLine(args ?? Array.Empty<string>())
                .AddJsonFile("appConfig.json", false);
            return configurationBuilder;
        }
    }
}
