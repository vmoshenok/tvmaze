﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;
using TvMaze.Common.Configuration.Models;

namespace TvMaze.Common.Configuration.Extensions
{
    public static class HostBuilderExtensions
    {
        public static IHostBuilder TvMazeHostBuild(this IHostBuilder hostBuilder, string[] args, string appName)
        {
            hostBuilder.ConfigureHostConfiguration(x => x.SetBasePath(AppDomain.CurrentDomain.BaseDirectory));

            hostBuilder.ConfigureAppConfiguration((_, configBuilder) =>
            {
                configBuilder.AddTvMazeConfiguration(args);
            });

            hostBuilder.ConfigureServices((_, serviceCollection) =>
            {
                serviceCollection.AddTvMazeServices();
            });

            hostBuilder.UseSerilog((_, services, configuration) =>
            {
                var appConfig = services.GetRequiredService<AppConfig>();
                var outputTemplate = "[{Timestamp:HH:mm:ss} {Level:u3} {id}] {Message:j}{NewLine}{Exception}";
                configuration
                    .MinimumLevel.Debug()
                    .Enrich.WithProperty("id", $"{Environment.MachineName}:{Environment.ProcessId}")
                    .WriteTo.Console(outputTemplate: outputTemplate)
                    .WriteTo.File($"./logs/log_{appName}_{appConfig.Env}.txt", LogEventLevel.Information, outputTemplate, rollingInterval: RollingInterval.Day);
            });

            return hostBuilder;
        }
    }
}
