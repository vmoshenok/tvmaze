﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyModel;
using Microsoft.Extensions.Logging;
using TvMaze.Common.Configuration.Models;

namespace TvMaze.Common.Configuration.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddTvMazeServices(this IServiceCollection serviceCollection)
        {
            serviceCollection.Scan(scan => scan.FromDependencyContext(DependencyContext.Default, x => x.FullName != null && x.FullName.Contains("TvMaze."))
                .AddClasses()
                .AsSelfWithInterfaces());
            serviceCollection.AddSingleton(x => new AppConfig(x.GetRequiredService<IConfiguration>()));
            serviceCollection.AddSingleton(x => x.GetRequiredService<ILoggerFactory>().CreateLogger("Default"));

            return serviceCollection;
        }
    }
}
