﻿using Newtonsoft.Json;

namespace TvMaze.Common.Configuration.Models
{
    public static class Settings
    {
        public static JsonSerializerSettings JsonSerializerSettings { get; set; } = new ()
        {
            Converters = new List<JsonConverter>
            {
                new JsonDateOnlyConverter()
            }
        };
    }
}
