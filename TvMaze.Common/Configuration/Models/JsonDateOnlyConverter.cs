﻿using Newtonsoft.Json;

namespace TvMaze.Common.Configuration.Models
{
    internal class JsonDateOnlyConverter:JsonConverter<DateOnly>
    {
        public override void WriteJson(JsonWriter writer, DateOnly value, JsonSerializer serializer)
        {
            writer.WriteValue(value.ToShortDateString());
        }

        public override DateOnly ReadJson(JsonReader reader, Type objectType, DateOnly existingValue, bool hasExistingValue,
            JsonSerializer serializer)
        {
            string s = (string)reader.Value;
            return DateOnly.TryParseExact(s, "yyyy-MM-dd", out var date) ? date : default;
        }
    }
}
