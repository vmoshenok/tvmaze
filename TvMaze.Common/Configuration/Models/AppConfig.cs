﻿using Microsoft.Extensions.Configuration;

namespace TvMaze.Common.Configuration.Models
{
    public sealed class AppConfig
    {
        private readonly IConfiguration _configuration;

        public AppConfig(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public string Env => _configuration["Env"] ?? "Dev";
        public string DbConnectionStr => _configuration["DbConnectionStr"];
        public string TvMazeApiUrl => _configuration["ApiEndpoints:TvMaze"];
        public int ParralelCrawlerTasks => int.TryParse(_configuration["ParralelCrawlerTasks"], out var result) ? result : 3;
    }
}
